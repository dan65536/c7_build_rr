FROM centos:7

RUN yum install -y epel-release sudo wget gcc-c++
RUN rpm --import https://core-dev.irods.org/irods-core-dev-signing-key.asc
RUN wget -qO - https://core-dev.irods.org/renci-irods-core-dev.yum.repo | tee /etc/yum.repos.d/renci-irods-core-dev.yum.repo
RUN yum install -y \
        irods-externals-clang-runtime6.0-0 \
        irods-externals-clang6.0-0 \
        irods-externals-cmake3.11.4-0
RUN yum install -y python3  ccache cmake make gcc gcc-c++ gdb libgcc libgcc.i686 \
                   glibc-devel glibc-devel.i686 libstdc++-devel libstdc++-devel.i686
RUN yum install -y capnproto capnproto-devel capnproto-libs ninja-build make git
RUN sudo pip3 install pexpect

## optional
RUN yum install -y vim-enhanced tig

WORKDIR /tmp
RUN mkdir obj
VOLUME  /tmp/obj
# Note: --- Last revision confirmed to build on CentOS 7 ---
#       commit 862605a8d4abca6d28d2296ccc6d6148ffc93ff6
#       Author: Keno Fischer <keno@juliacomputing.com>
#       Date:   Fri Mar 13 22:53:51 2020 -0400
RUN git clone http://github.com/mozilla/rr
RUN export LD_LIBRARY_PATH=/opt/irods-externals/clang-runtime6.0-0/lib \
           LD_RUN_PATH=/opt/irods-externals/clang-runtime6.0-0/lib     \
           CC=/opt/irods-externals/clang6.0-0/bin/clang \
           CXX="/opt/irods-externals/clang6.0-0/bin/clang++ -stdlib=libc++" && \
           cd obj && \
           /opt/irods-externals/cmake3.11.4-0/bin/cmake  ../rr -GNinja \
               -DCMAKE_INSTALL_PREFIX:PATH=/usr/local/rr && \
           /opt/irods-externals/cmake3.11.4-0/bin/cmake  --build . --target install
